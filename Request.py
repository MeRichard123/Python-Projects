import requests
from collections import namedtuple

class FailedToFetchResource(ValueError):
    pass
class FailedToDeleteResource(ValueError):
    pass
class FailedToMakeResource(ValueError):
    pass
class FailedToUpdateResource(ValueError):
    pass

class Request:
    def __init__(self, baseUrl):
        self.baseUrl = baseUrl

    def useNativeRequest(self: object, path: str, method: str, **kwargs):
        return requests.request(method,f"{self.baseUrl}{path}", *kwargs)


    def get(self: object, path: str, **kwargs):
        try:
            # Fetch the data using requests
            httpRequest = requests.get(f"{self.baseUrl}{path}", *kwargs)

            # Create a named tuple with all the keys in the dict of data from the request
            ResponseData = namedtuple("Data", ' '.join(httpRequest.json()[0].keys()))
            # Make an array for namedtuples for the data values for ease of access
            Data = [ResponseData(*data.values()) for data in httpRequest.json()]
            # Create a named tuple for the response for other data you might need
            httpResponse = namedtuple("Response", "status reason ok encoding headers url data")        
            return httpResponse(httpRequest.status_code,
                                httpRequest.reason,
                                httpRequest.ok,
                                httpRequest.ok,
                                httpRequest.headers,
                                httpRequest.url,
                                Data)
        except:
            self.isError = True
            raise FailedToFetchResource(f"{self.baseUrl}{path}")
    
    def delete(self: object, path: str, **kwargs):
        try:
            requests.delete(f"{self.baseUrl}{path}", *kwargs)
            Response = namedtuple("Response","msg status")
            return Response("Deleted Successfully", 204)
        except:
            self.isError = True
            raise FailedToDeleteResource(f"{self.baseUrl}{path}")

    def post(self: object, path: str, **kwargs):
        try:
            res = requests.post(f"{self.baseUrl}{path}", *kwargs)
            return res
        except:
            self.isError = True
            raise FailedToMakeResource(f"{self.baseUrl}{path}")

    def put(self: object, path: str, **kwargs):
        try:
            res = requests.put(f"{self.baseUrl}{path}", *kwargs)
            return res
        except:
            self.isError = True
            raise FailedToUpdateResource(f"{self.baseUrl}{path}")


if __name__ == "__main__":
    API = Request("https://jsonplaceholder.typicode.com")

    try:
        res = API.get("/posts")
        for post in res.data:
            print("\n===============")
            print(f"{post.id}) {post.title}")
            print(post.body)
            print("===============\n")

    except Exception as e:
        print(e)


