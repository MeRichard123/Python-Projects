from dataclasses import dataclass, asdict
from enum import Enum
from typing import NewType, Type, NoReturn, Union
from datetime import datetime
import re, csv, os

# ReGex for Number Plate Validation
carpattern = r'(^[A-Z]{2}[0-9]{2} [A-Z]{3}$)|(^[A-Z][0-9]{1,3} [A-Z]{3}$)|(^[A-Z]{3} [0-9]{1,3}[A-Z]$)|(^[0-9]{1,4} [A-Z]{1,2}$)|(^[0-9]{1,3} [A-Z]{1,3}$)|(^[A-Z]{1,2} [0-9]{1,4}$)|(^[A-Z]{1,3} [0-9]{1,3}$)'

Miles = NewType("miles", float)
DISTANCE_BETWEEN_SENSORS: Miles = 0.310686

mph = NewType("mpm", int)

class SanctionTypes(Enum):
    TICKET = 0
    WARNING = 1


@dataclass
class Road:
    name: str
    speedLimit: mph


@dataclass
class Sanction:
    type_: SanctionTypes
    contents: str
    date: str
    location: Type[Road]


@dataclass
class Car:
    registationPlate: str
    warnings: list


# More useful type annotation than just 'int'
Seconds = NewType("seconds", int)

# Custom Type Definitions
RoadType = Type[Road]
CarType = Type[Car]
SanctionType = Type[Sanction]

class TicketMachine:
    def __init__(self):
        self.Roads: list[RoadType] = []
        self.__CAR_DATABASE: dict[str, CarType] = {}
        self.initialise()

    def initialise(self) -> NoReturn:
        # Add All different Types of Roads to the Roads Array
        self.Roads.append(Road("Main Road", 30))
        self.Roads.append(Road("Derestricted Road", 60))
        self.Roads.append(Road("Motor Way", 70))

    def registerCarToDatabase(self, reg: str, car: CarType) -> NoReturn:
        self.__CAR_DATABASE[reg] = car

    def registerWarningToCar(self, reg: str, sanction: SanctionType) -> NoReturn:
        self.__CAR_DATABASE[reg].warnings.append(sanction)

    def findCar(self, reg: str) -> CarType:
        return self.__CAR_DATABASE[reg]

    def getIncidentRoad(self) -> RoadType:
        print("\nWhere was the incident?")
        for (index, road) in enumerate(self.Roads):
            print(f"{index+1}) {road.name}")

        roadCount = len(self.Roads)
        
        invalidRoad = True
        while invalidRoad:
            roadId = input(f"\nEnter an Option 1-{roadCount} ")
            if roadId.isnumeric():
                if int(roadId) <= roadCount and int(roadId) > 0:
                    invalidRoad = False
                else:
                    print("Please Enter a number within the provided limit") 
            else:
                print("Please Enter a number")
         
        return self.Roads[int(roadId)-1]

    def __calculateSpeed(self, timeBetweenSensors: Seconds) -> float:
        timeInHours = (timeBetweenSensors / 60) / 60
        return round(DISTANCE_BETWEEN_SENSORS / timeInHours, 2)

    def generateTicket(self, timeBetweenSensors: int, road: RoadType) -> Union[SanctionType, None]:
        currentSpeed = self.__calculateSpeed(timeBetweenSensors)
        print(f"\n Speed Was: {currentSpeed}mph \n")
        currentDate = datetime.now().strftime("%d/%m/%y - %H:%M:%S")
        
        upperBound = road.speedLimit * 1.1
        # Add an Extra 2mph leeway for motorways before issuing ticket
        if road.name == "Motor Way":
            upperBound += 2
            
        if currentSpeed > upperBound:
            return Sanction(SanctionTypes.TICKET, f"Above Speed Limit", currentDate, road)
        elif currentSpeed > road.speedLimit and currentSpeed < upperBound:
            return Sanction(SanctionTypes.WARNING, f"Above Limit but in leeway zone", currentDate, road)
        else:
            return None

    def checkCar(self, registrationPlate: str) -> bool:
        return registrationPlate in self.__CAR_DATABASE.keys()

    def PrintTicket(self, sanction: SanctionType) -> NoReturn:
        outputMsg = "Message: " + sanction.contents
        print("-"*len(outputMsg))
        print(sanction.type_.name.center(len(outputMsg), "="))
        print(outputMsg)
        print("Location: " + sanction.location.name)
        print("Date: " + sanction.date)
        print("-"*len(outputMsg)+"\n")

    def saveToFile(self,name:str) -> NoReturn:
        cars = list(map(lambda car: asdict(car) ,self.__CAR_DATABASE.values()))
        for car in cars:
            for warning in car['warnings']:
                warning['type_'] = warning['type_'].name
        fieldNames = ['registationPlate','warnings']
        try:
            with open(f"{name}.csv", "w") as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=fieldNames)
                writer.writeheader()
                writer.writerows(cars)
        except:
            print("Some Data Was Currupted Try Again")
        finally:
            print(f"\n == Successfully backed up Database to {name}.csv == \n")

    def loadDatabase(self, fileName:str) -> NoReturn:
        # this is the ugliest function I have ever written please ignore
        # it parses the CSV :(
        if not os.path.exists(f"{fileName}.csv"):
            print("File Doesn't Exist\n")
            return
        with open(f"{fileName}.csv", "r") as file:
            csvFile = csv.reader(file)
            filteredData = list(filter(lambda row:len(row),csvFile))
            for lines in filteredData[1:]:
                object_ = eval(lines[1])
                warnings = []
                for warning in object_:
                    if warning['type_'] == "TICKET":
                        type_ = SanctionTypes.TICKET
                    elif warning['type_'] == "WARNING":
                        type_ = SanctionTypes.WARNING
                    date = warning['date']
                    locData = warning['location']
                    location = Road(locData['name'], locData['speedLimit'])
                    sanction = Sanction(type_,warning['contents'],date, location)
                    warnings.append(sanction)
                car = Car(lines[0], warnings)
                self.registerCarToDatabase(lines[0], car)
        print(f"\n == Successfully Loaded Data from {fileName}.csv == \n")

def formatTimeInput() -> float:
    """
    time validation
    """
    unit = "s"
    unitNames = {'s':'seconds','m':'minutes','h':'hours'}
    print("\nWe need to record the time between sensors.")
    print("Which unit of time did you measure in?")
    print("- Seconds (s) [default]")
    print("- minutes (m)")
    print("- hours (h)")
    # get unit input
    invalidUnit = True
    while invalidUnit:
        units = input("Please enter a Unit for your input: ")
        if units == "":
            invalidUnit = False
        elif units in ['s','m','h']:
            invalidUnit = False
            unit = units
        else:
            print("Please enter a Valid Unit")

    # get time input
    invalidTime = True
    while invalidTime:
        time = input(f"\nHow many {unitNames[unit]} did the car take to pass the sensors? ")
        if time.replace('.', '', 1).isdigit():
            invalidTime = False
            time = float(time)
        else:
            print("Please for the love of god enter a number")

    if unit == "s":
        return time
    elif unit == "m":
        return time * 60
    elif unit == "h":
        return time * 3600
    


def Incident(machine: Type[TicketMachine]) -> NoReturn:
    roadType = machine.getIncidentRoad()
    
    invalidPlate = True
    while invalidPlate:
        carRegistration = input("\nWhat is the registration Plate of the Vehicle involved? ")
        if re.match(carpattern, carRegistration):
            invalidPlate = False
        else:
            print("That is not a Valid UK Plate")

    timeBetweenSensors = formatTimeInput()
    sanction = machine.generateTicket(timeBetweenSensors, roadType)
    if sanction:
        if machine.checkCar(carRegistration):
            machine.registerWarningToCar(carRegistration, sanction)
        else:
            newCar = Car(carRegistration, [sanction])
            machine.registerCarToDatabase(carRegistration, newCar)

        print(f"\n1 New Sanction issued to {carRegistration}...\n")
        machine.PrintTicket(sanction)
    else:
        print("No Sanctions Needed\n")


def CarLookUp(machine: Type[TicketMachine]) -> NoReturn:
    invalidPlate = True
    while invalidPlate:
        regNumber = input("\nWhat is the Registration of the Car you Wish to find? ")
        if re.match(carpattern, regNumber):
            invalidPlate = False
        else:
            print("That is not a Valid UK Plate")
    
    if machine.checkCar(regNumber):
        car = machine.findCar(regNumber)
        pluralise = 's' if len(car.warnings)>1 else ''
        print(f"\nThis Car has {len(car.warnings)} Reported Incident{pluralise}...\n")
        for sanction in car.warnings:
            machine.PrintTicket(sanction)
    else:
        print("No Records Exist for this Car\n")


def Main() -> NoReturn:
    machine = TicketMachine()
    option = -1
    print("Welcome Dear Traffic Warden...")
    print("Please Select an Option to Continue:\n")
    print("\nNOTE: Custom Number Plates are Currently Not Accepted.")
    print("Please use the old system for Custom Number Plates. Thank You \n")
    while option != "5":
        print("1. Register an Incident")
        print("2. Look Up a Car")
        print("3. Save Car Database to file")
        print("4. Load Database from file")
        print("5. Exit\n")
        
        option = input("Please Select an option from the Menu ")
        if option == "1":
            Incident(machine)
        elif option == "2":
            CarLookUp(machine)
        elif option == "3":
            invalidName = True
            while invalidName:
                print("Please DO NOT add File Extentions")
                fileName = input("What would you like to name the backup File? ")
                if not "." in fileName:
                    invalidName = False
                else:
                    print("I though I made it clear enough...\n")
            machine.saveToFile(fileName)
        elif option == "4":
            invalidName = True
            while invalidName:
                print("Please DO NOT add File Extentions")
                fileName = input("What is the name of the file you wish to load? ")
                if not "." in fileName:
                    invalidName = False
                else:
                    print("I though I made it clear enough...\n")
            machine.loadDatabase(fileName)


if __name__ == "__main__":
    Main()





