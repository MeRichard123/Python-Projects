import random
import time


start = time.time() # Start Timer

array = [5,7,8,10,225,1,89,12,4,65,7]

print("Sorting Bogosort...")
while array != sorted(array): # Check if the array is sorted 
    random.shuffle(array)     # Shuffle the array
#Keep shuffling until it's sorted
    
end = time.time() # End Timer 

print(array) 

# Find the time elapsed 
seconds = end-start

# If its less than 60 seconds print seconds
if seconds < 60:
     print(str(round(seconds,2))+"s")
# If it's more than 60 seconds calculate the minutes
else:
     seconds/= 60
     print(str(round(seconds,2)) + "m")
